function closestNumber(number: number, array: number[]): number {
  const closest = array.reduce((prev, curr) => {
    const diff = Math.abs(curr - number);
    const prevDiff = Math.abs(prev - number);
    return (diff < prevDiff && curr <= number ? curr : prev);
  });

  return closest;
}

export default closestNumber;
