import { StatusCodes } from 'http-status-codes';
import { Handler } from '../types';
import m2ConversorService from '../services/m2Conversor';

const m2Conversor : Handler = async (req, res, next) => {
  try {
    const { measurements } = req.body;

    const getArea = m2ConversorService(measurements);

    return res.status(StatusCodes.OK).json({ area: getArea });
  } catch (e) {
    next(e);
  }
};

export default m2Conversor;
