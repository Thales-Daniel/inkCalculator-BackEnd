import cors from 'cors';
import express from 'express';
import error from '../middlewares/error';

import inksRouter from '../router/inksRouter';
import m2ConversorRouter from '../router/m2Conversor';

import areaValidator from '../middlewares/areaValidator';
import verifyFields from '../middlewares/verifyFields';

const app = express();

app.use(cors());

app.use(express.json());

app.use('/inkCalculator', areaValidator, inksRouter);
app.use('/m2Conversor', verifyFields, m2ConversorRouter);

app.use(error);

export default app;
